from flask import Flask,render_template,abort
#import os
app = Flask(__name__)

'''
source:http://flask.pocoo.org/docs/1.0/quickstart/
for 403/404 function and abort
'''
@app.route("/<path:name>")

def webname(name):
    #same as first project, first check .html and css, if it is true, then go to next step
    if name.endswith("html") or name.endswith("css"):
        #use try and except to check "..","~" and "//" and whether the web exist in templates
        try:
            return render_template(name),200
        except:
            if  ".."in name or "~"in name or "//" in name:
                abort(403)
            
                
            else:
                abort(404)
    else:
        abort(403)

@app.errorhandler(403)
def Forbidden(F):
    return render_template("403.html"),403
@app.errorhandler(404)
def Notfound(F):
    return render_template("404.html"),404
        
    

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
